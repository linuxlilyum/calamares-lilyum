%define calamares_dir %{buildroot}%{_sysconfdir}/calamares 
Name:           calamares-lilyum
Version:        0.55
Release:        0
Summary:        Lilyum theme and settings for the Calamares Installer
License:        GPL-2.0+
Group:          System/Management
Url:            https://github.com/linuxlilyum/%{name}
Source:         https://gitlab.com/linuxlilyum/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  libqt5-linguist

Requires:       calamares
#Requires:       calamares-branding-upstream


%description
This package contains settings and branding about Calamares for Lilyum Project. 

%prep
%setup -q


%build
cd branding/Lilyum/lang/
lrelease-qt5 *.ts
rm *.ts


%install
mkdir -p %{calamares_dir}
for i in branding modules settings.conf; do 
    cp -r $i %{calamares_dir}
done

%files
%{_sysconfdir}/calamares/
%license LICENSE
%doc README.md

%changelog
